/**
 * 
 */
package danyfel80.roi.coloring;

import java.awt.Color;

import icy.roi.ROI;
import icy.util.Random;

/**
 * ROI colorizer. Takes an array of ROIs and sets the color of each ROI in a random way.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class RoiRandomColorizer
{

    /**
     * Sets the color of the target ROIs randomly with 50% of saturation and 100% brightness.
     * 
     * @param targetRois
     *        ROIs to colorize.
     */
    public static void colorize(ROI[] targetRois)
    {
        colorize(targetRois, 0.5f, 1f);
    }

    /**
     * Sets the color of the target ROIs randomly with a given saturation and brightness level.
     * 
     * @param targetRois
     *        ROIs to colorize.
     * @param saturation
     *        Saturation level (0.0-1.0).
     * @param brightness
     *        Brightness level (0.0-1.0).
     */
    public static void colorize(ROI[] targetRois, float saturation, float brightness)
    {
        for (ROI roi : targetRois)
        {
            roi.setColor(new Color(Color.HSBtoRGB(Random.nextFloat(), saturation, brightness)));
        }
    }

}
