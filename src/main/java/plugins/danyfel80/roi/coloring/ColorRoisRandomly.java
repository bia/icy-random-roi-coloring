/**
 * 
 */
package plugins.danyfel80.roi.coloring;

import danyfel80.roi.coloring.RoiRandomColorizer;
import icy.main.Icy;
import icy.plugin.PluginLauncher;
import icy.plugin.PluginLoader;
import icy.roi.ROI;
import icy.system.IcyHandledException;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarROIArray;

/**
 * Assigns to the ROIs passed to this plugin different random colors.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class ColorRoisRandomly extends EzPlug implements Block
{

    public static void main(String[] args)
    {
        Icy.main(args);
        PluginLauncher.start(PluginLoader.getPlugin(ColorRoisRandomly.class.getName()));
    }

    private VarROIArray varInRois;

    @Override
    public void declareInput(VarList inputMap)
    {
        varInRois = new VarROIArray("Target ROI(s)");
        inputMap.add("inRois", varInRois);
    }

    private VarROIArray varOutRois;

    @Override
    public void declareOutput(VarList outputMap)
    {
        varOutRois = new VarROIArray("Colored ROI(s)");
        outputMap.add("outRois", varOutRois);
    }

    private EzVarSequence varInSequence;
    private EzVarBoolean varInSelectedRois;

    @Override
    protected void initialize()
    {
        varInSequence = new EzVarSequence("Sequence");
        varInSelectedRois = new EzVarBoolean("Selected ROIs", true);
        addEzComponent(varInSequence);
        addEzComponent(varInSelectedRois);
    }

    @Override
    protected void execute()
    {
        try
        {
            retrieveTargetRois();
            applyColors();
            setOutput();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new IcyHandledException("Error coloring rois: " + e.getMessage(), e);
        }
        finally
        {
            cleanReferences();
        }
    }

    private ROI[] targetRois;

    private void retrieveTargetRois()
    {
        targetRois = (!isHeadLess())
            ? (varInSelectedRois.getValue()
                ? varInSequence.getValue(true).getSelectedROIs().toArray(new ROI[0])
                : varInSequence.getValue(true).getROIs().toArray(new ROI[0]))
            : varInRois.getValue();
    }

    private void applyColors()
    {
        RoiRandomColorizer.colorize(targetRois);
    }

    private void setOutput()
    {
        if (isHeadLess())
        {
            varOutRois.setValue(targetRois);
        }
    }

    private void cleanReferences()
    {
        targetRois = null;
    }

    @Override
    public void clean()
    {
    }

}
